import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let {name, price, image} = this.props.shoe;
    return (
      <div className='col-4 p-3 shoeItem'>
        <div className="card border border-primary px-2">
            <img src={image} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text itemPrice">${price}</p>
                <br />
                <button onClick={() => {this.props.handleAddToCart(this.props.shoe)}} className="btn btn-primary">Add to cart</button>
            </div>
        </div>
      </div>
    )
  }
}
