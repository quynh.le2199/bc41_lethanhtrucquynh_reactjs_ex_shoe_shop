import React, { Component } from 'react'
import ListShoe from './ListShoe'
import { data_shoe } from './data_shoe'
import CartShoe from './CartShoe'

export default class Ex_Shoe_Shop extends Component {
    state = {
        listShoe: data_shoe,
        cart: [],
    }
    handleAddToCart = (shoe) => {
        let cloneCart = [...this.state.cart];
        let index = cloneCart.findIndex((item) => {
            return item.id == shoe.id;
        })
        if (index == -1) {
            let newShoe = {...shoe, soLuong: 1};
            cloneCart.push(newShoe);
        }
        else {
            cloneCart[index].soLuong++;
        }
        this.setState({cart: cloneCart});
    }
    handleDelete = (idShoe) => {
        let newCart = this.state.cart.filter((item) => {
            return item.id != idShoe;
        })
        this.setState({cart: newCart})
    }
    handleChangeQuantity = (idShoe, payload) => {
        let cloneCart = [...this.state.cart]
        let index = cloneCart.findIndex((item) => {
            return item.id == idShoe;
        })
        cloneCart[index].soLuong += payload;
        this.setState({cart: cloneCart});
    }
  render() {
    return (
      <div className='container'>
        <h2 className='my-3'>Ex_Shoe_Shop</h2>
        {this.state.cart.length > 0 && <CartShoe cart={this.state.cart} handleDelete={this.handleDelete} handleChangeQuantity={this.handleChangeQuantity}/>}
        <ListShoe list={this.state.listShoe} handleAddToCart={this.handleAddToCart}/>
      </div>
    )
  }
}
